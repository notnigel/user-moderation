@extends('layouts.app')

@section('content')
    <div class="container">
    <br />
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Description</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($posts as $post)
      <tr>
        <td>{{$post->id}}</td>
        <td>{{$post->title}}</td>
        <td>{{$post->description}}</td>
        <td>
        @if($post->status == 0)
        <span class="label label-primary">Pending</span>
        @elseif($post->status == 1)
        <span class="label label-success">Approved</span>
        @elseif($post->status == 2)
        <span class="label label-danger">Rejected</span>
        @else
        <span class="label label-info">Postponed</span>
       @endif
        </td>
        <td><a href="{{action('PostController@edit', $post->id)}}" class="btn btn-warning">Moderate</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
@endsection
