@extends('layouts.app')

@section('content')
<div class="container">
  <h5>Create Event</h5><br/>
  <div class="container">
    @if(\Session::has('success'))
    <div class="alert alert-success">
      {{\Session::get('success')}}
    </div>
    @endif
  </div>
  <form method="post" action="{{url('post')}}">
    @csrf
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <label for="Contact Name">Contact Name:</label>
        <input type="text" class="form-control" name="contactname">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <label for="Contact Email">Contact Email:</label>
        <input type="email" class="form-control" name="contactemail">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <label for="Title">Title:</label>
        <input type="text" class="form-control" name="title">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <label for="Description">Description:</label>
        <textarea class="form-control" name="description" rows="5"></textarea>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <label for="Date">Date:</label>
        <input type="date" class="form-control" name="date">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <label for="Event or RSVP Link">Event or RSVP Link:</label>
        <input type="url" class="form-control" name="eventlink">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <button type="submit" class="btn btn-success">Submit</button>
      </div>
    </div>
  </form>
</div>
@endsection
