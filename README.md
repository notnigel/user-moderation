If you have PHP installed locally and you would like to use PHP's built-in development server to serve your application, you may use the serve Artisan command. 

This command will run the database migrations:
> php artisan migrate

You can create the first user in the database manually and the rest can be done from the UI.

This command will start a development server at http://localhost:8000

> php artisan serve

The login page can be accessed http://localhost:8080/login