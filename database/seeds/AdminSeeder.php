<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Nigel D',
            'email' => 'nigel.a.david@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('nigel123'),
            'admin' => 1,
            'approved_at' => now(),
        ]);
    }
}
